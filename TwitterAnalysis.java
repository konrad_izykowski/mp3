/**
 * 
 * TwitterAnalysis.java takes in twitter data from a text file, takes 
 * in queries about that data from another text file, and outputs the 
 * answers to those questions in a third text file
 * @author Konrad Izykowski 26506148 
 * @author Zeyad Tamimi 12504149
 *
 */


package ca.ubc.ece.cpen221.mp3.graph;

import java.util.*;
import java.io.*;

import ca.ubc.ece.cpen221.mp3.staff.Graph;
import ca.ubc.ece.cpen221.mp3.staff.Vertex;

public class TwitterAnalysis {
    
    /**
     * 
     * Takes queries about twitter data and outputs the answers
     * 
     * @param args - contains the address of the file from which to read queries in args[0]
     *             - contains the address of the file in which to answer queries in args[1]
     */
    public static void main(String [ ] args){
        //where the graph data is stored
        String dataAddress = args[0];
        //where the query data is stored
        String queryAddress = args[1];
        //where the answer data is stored
        String answerAddress = args[2];
        
        //examples of arg[0], arg[1], arg[2]
//      String dataAddress = "datasets/twitter.txt";
//      String queryAddress = "datasets/queries.txt";
//      String answerAddress = "datasets/answers.txt";
        
        Graph twitterData;
        
        //puts data from twitter data text file into a graph
        twitterData = getDataIntoGraph(dataAddress);
        
        //puts data from query data text file into a List<String[]>
        List<String[]> queryData = getQueries(queryAddress);
        
        //puts answers to queries into a data text file
        queryAnswersToFile(twitterData, queryData, answerAddress);
    }
    
    
    /**
     * 
     * Reads twitter data from a file and returns a graph containing it
     * 
     * @param the string containing the address of the twitter data file
     * @throws RuntimeException - if the address does not point to an existing text file
     *                          - if there is an issue reading data from the text file
     * @returns a graph containing all the data read from the twitter data file
     */
    private static Graph getDataIntoGraph(String dataFileNameAndAddress){
        Graph graphOfData = new AdjacencyListGraph();
        FileInputStream twitterStream;
        
        try {
            twitterStream = new FileInputStream(dataFileNameAndAddress);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
        try {
            BufferedReader twitterReader = new BufferedReader(
                    new InputStreamReader(twitterStream));
            
            Set<Vertex> addedVertices = new HashSet<Vertex>();
            
            String line;
            
            while ((line = twitterReader.readLine()) != null) {
         
                String[] columns = line.split(" -> ");

                // After the split, we should have the following (as Strings):
                // - columns[0] contains a,
                // - columns[1] contains b
                // a follows b
                // a downstream of b (flow of tweets)
                // edge from b to a

                Vertex downstream = new Vertex(columns[0]);
                Vertex upstream = new Vertex(columns[1]);
                
                //only adds a vertex to the graph once
                if(!addedVertices.contains(downstream)){
                    graphOfData.addVertex(downstream);
                    addedVertices.add(downstream);
                }
                
                //only adds a vertex to the graph once
                if(!addedVertices.contains(upstream)){
                    graphOfData.addVertex(upstream);
                    addedVertices.add(upstream);
                }
                
                //only adds an edge to the graph once 
                if(!graphOfData.edgeExists(upstream, downstream)){
                    graphOfData.addEdge(upstream, downstream);
                }    
            }
            
            twitterReader.close();
            twitterStream.close();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return graphOfData;
    }
    
    /**
     * 
     * Reads queries from a file and returns a List<String[]> containing them
     * 
     * @param the string containing the address of the query text file
     * @throws RuntimeException - if the address does not point to an existing text file
     *                          - if there is an issue reading data from the text file
     * @returns a List<String[]> - containing all the queries inside the data file
     *                           - no queries are duplicated
     */
    private static List<String[]> getQueries(String queryFileNameAndAddress){
        List<String[]> queries = new ArrayList<String[]>();
        
        FileInputStream queryStream;

        try {
            queryStream = new FileInputStream(queryFileNameAndAddress);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
        try {
            BufferedReader queryReader = new BufferedReader(
                    new InputStreamReader(queryStream));
            
            String line;
            
            while ((line = queryReader.readLine()) != null) {

                String[] columns = line.split(" ");

                // After the split, we should have the following (as Strings):
                // - columns[0] contains query type (commonInfluencers or numRetweets)
                // - columns[1] contains user a
                // - columns[2] contains user b
                // - columns[3] should contain ?
                
                
                //only adds query if columns[3] is "?"
                if(columns.length == 4 && columns[3].equals("?")){
                    String[] currentQuery = Arrays.copyOfRange(columns, 0, 3);
                    
                    //only adds query if hasn't occurred before
                    if(!queries.contains(currentQuery)){
                        queries.add(currentQuery);
                    }
                }

            }
            queryReader.close();
            queryStream.close();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        
        return queries;
    }
    
    /**
     * 
     * Writes answers to the queries based on the twitter data to a text file
     * 
     * @param graphToAnalyze - the graph containing twitter data
     * @param queries - the questions about twitter data that are answered
     * @param the string containing the address text file to write answers to
     * @throws RuntimeException if there is an issue writing data to the text file
     */
    private static void queryAnswersToFile(Graph graphToAnalyze, 
            List<String[]> queries, String answerFileNameAndAddress){
        
        Vertex userA, userB;
        List<Vertex> commonInfluencers;
        Integer numOfRetweets;
        String queryType1 = "commonInfluencers";
        String queryType2 = "numRetweets";
        String tab = "    ";
        List<Vertex> allVertices = graphToAnalyze.getVertices();
        
        File answerFile = new File(answerFileNameAndAddress);
        FileOutputStream answerStream;

        try {
            answerStream = new FileOutputStream(answerFile);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
        try {
            BufferedWriter answerWriter = new BufferedWriter(
                    new OutputStreamWriter(answerStream));
            
            //everything up to this is file IO
            
            for(String[] queryCurrent : queries){

                userA = new Vertex(queryCurrent[1]);
                userB = new Vertex(queryCurrent[2]);
                
                //only answers the query if both Vertices are in the graph
                if(allVertices.contains(userA) & allVertices.contains(userB)){
                    
                    //if the query is of type commonInfluencers
                    if(queryCurrent[0].equals(queryType1)){
                        
                        //uses Algorithm.commonUpstream()
                        commonInfluencers = Algorithms.commonUpstream(graphToAnalyze, userA, userB);
                        
                        answerWriter.write("query: " + queryType1 + " " + 
                                queryCurrent[1] + " " + queryCurrent[2]);
                        answerWriter.newLine();
                        answerWriter.write("<result>");
                        answerWriter.newLine();
                        
                        for(Vertex vertexToPrint : commonInfluencers){
                            answerWriter.write(tab + vertexToPrint.getLabel());
                            answerWriter.newLine();
                        }
                        
                        answerWriter.write("</result>");
                        answerWriter.newLine();
                        answerWriter.newLine();
                    }
                    
                    //uses Algorithm.shortestDistance()
                    else if(queryCurrent[0].equals(queryType2)){
                        
                        answerWriter.write("query: " + queryType2 + " " + 
                                queryCurrent[1] + " " + queryCurrent[2]);
                        answerWriter.newLine();
                        answerWriter.write("<result>");
                        answerWriter.newLine();
                        
                        try{
                            numOfRetweets = Algorithms.shortestDistance(graphToAnalyze, userA, userB);
                            answerWriter.write(numOfRetweets.toString());
                            answerWriter.newLine();
                        } catch (PathNotFoundException e) {
                            answerWriter.write(tab + "no path found");
                            answerWriter.newLine();
                        }
                        
                        answerWriter.write("</result>");
                        answerWriter.newLine();
                        answerWriter.newLine();
                    }
                }
            }
            
            answerWriter.close();
            answerStream.close();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
