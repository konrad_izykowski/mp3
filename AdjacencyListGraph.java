/**
 * 
 * This function constructs an AdjacencyListGraph object that implements Graph
 * and conducts various operations with that Graph object.
 * @author Konrad Izykowski 26506148 
 * @author Zeyad Tamimi 12504149
 *
 */
package ca.ubc.ece.cpen221.mp3.graph;

import java.util.*;
import java.util.Map.Entry;

import ca.ubc.ece.cpen221.mp3.staff.Graph;
import ca.ubc.ece.cpen221.mp3.staff.Vertex;

public class AdjacencyListGraph implements Graph{

    private Map<Vertex, List<Vertex>> adjacencyList = new HashMap<Vertex, List<Vertex>>();

    /**
     * Adds a vertex to the graph.
     *  
     * @param the vertex to be added to the graph
     * @requires v is not already a vertex in the graph
     */
    public void addVertex(Vertex v) {
        List<Vertex> empty = new ArrayList<Vertex>();

        adjacencyList.put(v, empty);
    }

    /**
     * Adds an edge from v1 to v2.
     *
     * @param v1 - the vertex where the edge starts
     * @param v2 - the vertex where the edge ends
     * @requires v1 and v2 are vertices in the graph
     */
    public void addEdge(Vertex v1, Vertex v2) {
        adjacencyList.get(v1).add(v2);
    }

    /**
     * Checks if there is an edge from v1 to v2.
     * 
     * @param v1 - the vertex where the edge should start
     * @param v2 - the vertex where the edge should end
     * @requires v1 and v2 are vertices in the graph
     * @returns true if there is an edge starting at v1 and ending at v2
     */
    public boolean edgeExists(Vertex v1, Vertex v2) {
        
    	List<Vertex> edgeTest = adjacencyList.get(v1);
    	
    	for(Vertex testVertex : edgeTest)
    	{
    		if (testVertex.equals(v2))
    			return true;
    	}
        
        return false;
    }

    /**
     * Gets an array containing all downstream vertices adjacent to v.
     *
     * @param the vertex for which all downstream vertices are retrieved
     * @requires v is a vertex in the graph
     * @returns - a List of all and only the Vertices downstream of v
     *          - the List will not have any trailing null values
     *          - these are references so the vertexes can be edited
     */
    public List<Vertex> getDownstreamNeighbors(Vertex v) {
    	if (adjacencyList.containsKey(v))
    		return adjacencyList.get(v);
    	else
    		return new ArrayList<Vertex>();
    }

    /**
     * Get an array containing all upstream vertices adjacent to v.
     *
     * @param the vertex for which all upstream vertices are retrieved
     * @requires v is a vertex in the graph
     * @returns - a List of all and only the Vertices upstream of v
     *          - the List will not have any trailing null values
     *          - these are references so the vertexes can be edited
     */
    public List<Vertex> getUpstreamNeighbors(Vertex v) {
        List<Vertex> upStream = new ArrayList<Vertex>();
        Entry<Vertex, List<Vertex>> current;

        Iterator<Entry<Vertex, List<Vertex>>> itr = adjacencyList.entrySet().iterator();

        while (itr.hasNext()) {
            current = itr.next();
            List<Vertex> downstream = current.getValue();

            if (downstream.contains(v)) {
                upStream.add(current.getKey());
            }
        }
        
        return upStream;
    }

    /**
     * Get all vertices in the graph.
     *
     * @returns - returns a list containing all and only the vertices in the graph. 
     *          - this list will have no trailing null values
     *          - these are references so the vertexes can be edited
     */
    public List<Vertex> getVertices() {
        List<Vertex> keySetToList = new ArrayList<Vertex>();
        
        keySetToList.addAll(adjacencyList.keySet());
        
        return keySetToList;
    }

}
