/**
 * 
 * This file contains the definition of the PathNotFoundException
 * @author Konrad Izykowski 26506148 
 * @author Zeyad Tamimi 12504149
 *
 */
package ca.ubc.ece.cpen221.mp3.graph;

public class PathNotFoundException  extends Exception {}
