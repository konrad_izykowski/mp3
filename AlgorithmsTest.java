/**
 * 
 * This jUnit tests Algorithms.java which tests distance, BFS, DFS, common upstream, 
 * and common downstream on both adjacency list graphs and adjacency matrix graphs
 * @author Konrad Izykowski 26506148 
 * @author Zeyad Tamimi 12504149
 *
 */
package ca.ubc.ece.cpen221.mp3.tests;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.*;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import ca.ubc.ece.cpen221.mp3.graph.AdjacencyListGraph;
import ca.ubc.ece.cpen221.mp3.graph.AdjacencyMatrixGraph;
import ca.ubc.ece.cpen221.mp3.graph.Algorithms;
import ca.ubc.ece.cpen221.mp3.graph.PathNotFoundException;
import ca.ubc.ece.cpen221.mp3.staff.Graph;
import ca.ubc.ece.cpen221.mp3.staff.Vertex;

public class AlgorithmsTest {

	// Setting up all adjacency list Graph Test Cases
	private final Graph testgraph_empty = new AdjacencyListGraph();
	private final Graph testgraph_one = new AdjacencyListGraph();
	private final Graph testgraph_two = new AdjacencyListGraph();
	private final Graph testgraph_three = new AdjacencyListGraph();
	private final Graph testgraph_four = new AdjacencyListGraph();
	private final Graph testgraph_five = new AdjacencyListGraph();
	private final Graph testgraph_six = new AdjacencyListGraph();
	private final Graph testgraph_seven = new AdjacencyListGraph();

	// Setting up all adjacency Matrix Graph Test Cases
	private final Graph M_testgraph_empty = new AdjacencyMatrixGraph();
	private final Graph M_testgraph_one = new AdjacencyMatrixGraph();
	private final Graph M_testgraph_two = new AdjacencyMatrixGraph();
	private final Graph M_testgraph_three = new AdjacencyMatrixGraph();
	private final Graph M_testgraph_four = new AdjacencyMatrixGraph();
	private final Graph M_testgraph_five = new AdjacencyMatrixGraph();
	private final Graph M_testgraph_six = new AdjacencyMatrixGraph();
	private final Graph M_testgraph_seven = new AdjacencyMatrixGraph();

	// Setting Up Vertices
	private final Vertex v1 = new Vertex("v1");
	private final Vertex v2 = new Vertex("v2");
	private final Vertex v3 = new Vertex("v3");
	private final Vertex v4 = new Vertex("v4");
	private final Vertex v5 = new Vertex("v5");
	private final Vertex v6 = new Vertex("v6");
	private final Vertex v7 = new Vertex("v7");
	private final Vertex v8 = new Vertex("v8");
	private final Vertex v9 = new Vertex("v9");
	private final Vertex v10 = new Vertex("v10");

	// Generating the graphs to be used
	@Before
	public void setUp() throws Exception {

		// Setting up graph 1 1 edge

		testgraph_one.addVertex(v1);
		testgraph_one.addVertex(v2);
		testgraph_one.addEdge(v1, v2);

		M_testgraph_one.addVertex(v1);
		M_testgraph_one.addVertex(v2);
		M_testgraph_one.addEdge(v1, v2);

		// Setting up graph 2 2 looping edges

		testgraph_two.addVertex(v1);
		testgraph_two.addVertex(v2);
		testgraph_two.addEdge(v1, v2);
		testgraph_two.addEdge(v2, v1);

		M_testgraph_two.addVertex(v1);
		M_testgraph_two.addVertex(v2);
		M_testgraph_two.addEdge(v1, v2);
		M_testgraph_two.addEdge(v2, v1);

		// Setting Up Graph 3 2 branches

		testgraph_three.addVertex(v1);
		testgraph_three.addVertex(v2);
		testgraph_three.addVertex(v3);
		testgraph_three.addEdge(v1, v2);
		testgraph_three.addEdge(v1, v3);

		M_testgraph_three.addVertex(v1);
		M_testgraph_three.addVertex(v2);
		M_testgraph_three.addVertex(v3);
		M_testgraph_three.addEdge(v1, v2);
		M_testgraph_three.addEdge(v1, v3);

		// Setting Up Graph 4 multiple branches

		testgraph_four.addVertex(v1);
		testgraph_four.addVertex(v2);
		testgraph_four.addVertex(v3);
		testgraph_four.addVertex(v4);
		testgraph_four.addVertex(v5);
		testgraph_four.addEdge(v1, v2);
		testgraph_four.addEdge(v2, v3);
		testgraph_four.addEdge(v2, v4);
		testgraph_four.addEdge(v2, v5);

		M_testgraph_four.addVertex(v1);
		M_testgraph_four.addVertex(v2);
		M_testgraph_four.addVertex(v3);
		M_testgraph_four.addVertex(v4);
		M_testgraph_four.addVertex(v5);
		M_testgraph_four.addEdge(v1, v2);
		M_testgraph_four.addEdge(v2, v3);
		M_testgraph_four.addEdge(v2, v4);
		M_testgraph_four.addEdge(v2, v5);

		// Setting Up Graph 5 vertices with multiple upstream / downstream

		testgraph_five.addVertex(v1);
		testgraph_five.addVertex(v2);
		testgraph_five.addVertex(v3);
		testgraph_five.addVertex(v4);
		testgraph_five.addEdge(v1, v2);
		testgraph_five.addEdge(v1, v3);
		testgraph_five.addEdge(v4, v2);
		testgraph_five.addEdge(v4, v3);

		M_testgraph_five.addVertex(v1);
		M_testgraph_five.addVertex(v2);
		M_testgraph_five.addVertex(v3);
		M_testgraph_five.addVertex(v4);
		M_testgraph_five.addEdge(v1, v2);
		M_testgraph_five.addEdge(v1, v3);
		M_testgraph_five.addEdge(v4, v2);
		M_testgraph_five.addEdge(v4, v3);

		// Setting Up Graph 6 more complicated graph 4

		testgraph_six.addVertex(v1);
		testgraph_six.addVertex(v2);
		testgraph_six.addVertex(v3);
		testgraph_six.addVertex(v4);
		testgraph_six.addVertex(v5);
		testgraph_six.addVertex(v6);
		testgraph_six.addEdge(v1, v2);
		testgraph_six.addEdge(v2, v3);
		testgraph_six.addEdge(v2, v4);
		testgraph_six.addEdge(v2, v5);
		testgraph_six.addEdge(v4, v6);

		M_testgraph_six.addVertex(v1);
		M_testgraph_six.addVertex(v2);
		M_testgraph_six.addVertex(v3);
		M_testgraph_six.addVertex(v4);
		M_testgraph_six.addVertex(v5);
		M_testgraph_six.addVertex(v6);
		M_testgraph_six.addEdge(v1, v2);
		M_testgraph_six.addEdge(v2, v3);
		M_testgraph_six.addEdge(v2, v4);
		M_testgraph_six.addEdge(v2, v5);
		M_testgraph_six.addEdge(v4, v6);

		// Setting Up Graph 7: Multi Start Point Case

		testgraph_seven.addVertex(v1);
		testgraph_seven.addVertex(v2);
		testgraph_seven.addVertex(v3);
		testgraph_seven.addVertex(v4);
		testgraph_seven.addVertex(v5);
		testgraph_seven.addVertex(v6);
		testgraph_seven.addVertex(v7);
		testgraph_seven.addVertex(v8);
		testgraph_seven.addVertex(v9);
		testgraph_seven.addVertex(v10);
		testgraph_seven.addEdge(v1, v2);
		testgraph_seven.addEdge(v2, v3);
		testgraph_seven.addEdge(v2, v4);
		testgraph_seven.addEdge(v4, v5);
		testgraph_seven.addEdge(v4, v6);
		testgraph_seven.addEdge(v6, v7);
		testgraph_seven.addEdge(v8, v9);
		testgraph_seven.addEdge(v9, v4);
		testgraph_seven.addEdge(v9, v6);
		testgraph_seven.addEdge(v9, v10);

		M_testgraph_seven.addVertex(v1);
		M_testgraph_seven.addVertex(v2);
		M_testgraph_seven.addVertex(v3);
		M_testgraph_seven.addVertex(v4);
		M_testgraph_seven.addVertex(v5);
		M_testgraph_seven.addVertex(v6);
		M_testgraph_seven.addVertex(v7);
		M_testgraph_seven.addVertex(v8);
		M_testgraph_seven.addVertex(v9);
		M_testgraph_seven.addVertex(v10);
		M_testgraph_seven.addEdge(v1, v2);
		M_testgraph_seven.addEdge(v2, v3);
		M_testgraph_seven.addEdge(v2, v4);
		M_testgraph_seven.addEdge(v4, v5);
		M_testgraph_seven.addEdge(v4, v6);
		M_testgraph_seven.addEdge(v6, v7);
		M_testgraph_seven.addEdge(v8, v9);
		M_testgraph_seven.addEdge(v9, v4);
		M_testgraph_seven.addEdge(v9, v6);
		M_testgraph_seven.addEdge(v9, v10);

	}

	// Testing BFS for List
	@Test
	public void testBFSEmpty() {
		Set test = Algorithms.BreadthFirstSearch(testgraph_empty);
		
		assertEquals(test.size(), 0);
	}

	@Test
	public void testBFSOneEdge() {
		Set<List<Vertex>> test = Algorithms.BreadthFirstSearch(testgraph_one);
		Queue<Vertex> Expected = new LinkedList<Vertex>();
		
		Expected.add(v1);
		Expected.add(v2);
		Expected.add(v2);
		
		assertEquals(test.size(), 2);

		for (List<Vertex> ResultList : test) {
			for (Vertex ResultElement : ResultList) {
				assertTrue(ResultElement.equals(Expected.remove()));
			}
		}
	}

	@Test
	public void testBFSTwoLoopingEdges() {
		Set<List<Vertex>> test = Algorithms.BreadthFirstSearch(testgraph_two);
		Queue<Vertex> Expected = new LinkedList<Vertex>();

		Expected.add(v2);
		Expected.add(v1);
		Expected.add(v1);
		Expected.add(v2);

		assertEquals(test.size(), 2);

		for (List<Vertex> ResultList : test) {
			for (Vertex ResultElement : ResultList) {
				assertTrue(ResultElement.equals(Expected.remove()));
			}
		}
	}

	@Test
	public void testBFSTwoBranches() {
		Set<List<Vertex>> test = Algorithms.BreadthFirstSearch(testgraph_three);
		Queue<Vertex> Expected = new LinkedList<Vertex>();
		
		Expected.add(v1);
		Expected.add(v2);
		Expected.add(v3);
		Expected.add(v2);
		Expected.add(v3);
		
		assertEquals(test.size(), 3);

		for (List<Vertex> ResultList : test) {
			for (Vertex ResultElement : ResultList) {
				assertTrue(ResultElement.equals(Expected.remove()));
			}
		}
	}

	@Test
	public void testBFSMultipleBranches() {
		Set<List<Vertex>> test = Algorithms.BreadthFirstSearch(testgraph_four);
		List<List<Vertex>> Expected = new ArrayList<List<Vertex>>();
		List<Vertex> Expectedinside = new ArrayList<Vertex>();

		assertEquals(test.size(), 5);
		
		Expectedinside.add(v5);
		Expected.add(Expectedinside);
		Expectedinside = new ArrayList<Vertex>();
		Expectedinside.add(v4);
		Expected.add(Expectedinside);
		Expectedinside = new ArrayList<Vertex>();
		Expectedinside.add(v3);
		Expected.add(Expectedinside);
		Expectedinside = new ArrayList<Vertex>();
		Expectedinside.add(v1);
		Expectedinside.add(v2);
		Expectedinside.add(v3);
		Expectedinside.add(v4);
		Expectedinside.add(v5);
		Expected.add(Expectedinside);
		Expectedinside = new ArrayList<Vertex>();
		Expectedinside.add(v2);
		Expectedinside.add(v3);
		Expectedinside.add(v4);
		Expectedinside.add(v5);
		Expected.add(Expectedinside);

		for (List<Vertex> EpectedList : Expected) {
			assertTrue(test.contains(EpectedList));
		}
	}

	@Test
	public void testBFSMultiStart() {
		Set<List<Vertex>> test = Algorithms.BreadthFirstSearch(testgraph_seven);
		List<List<Vertex>> Expected = new ArrayList<List<Vertex>>();
		List<Vertex> Expectedinside = new ArrayList<Vertex>();
		
		assertEquals(test.size(), 10);

		Expectedinside.add(v1);
		Expectedinside.add(v2);
		Expectedinside.add(v3);
		Expectedinside.add(v4);
		Expectedinside.add(v5);
		Expectedinside.add(v6);
		Expectedinside.add(v7);
		Expected.add(Expectedinside);
		Expectedinside = new ArrayList<Vertex>();
		Expectedinside.add(v2);
		Expectedinside.add(v3);
		Expectedinside.add(v4);
		Expectedinside.add(v5);
		Expectedinside.add(v6);
		Expectedinside.add(v7);
		Expected.add(Expectedinside);
		Expectedinside = new ArrayList<Vertex>();
		Expectedinside.add(v3);
		Expected.add(Expectedinside);
		Expectedinside = new ArrayList<Vertex>();
		Expectedinside.add(v4);
		Expectedinside.add(v5);
		Expectedinside.add(v6);
		Expectedinside.add(v7);
		Expected.add(Expectedinside);
		Expectedinside = new ArrayList<Vertex>();
		Expectedinside.add(v6);
		Expectedinside.add(v7);
		Expected.add(Expectedinside);
		Expectedinside = new ArrayList<Vertex>();
		Expectedinside.add(v7);
		Expected.add(Expectedinside);
		Expectedinside = new ArrayList<Vertex>();
		Expectedinside.add(v8);
		Expectedinside.add(v9);
		Expectedinside.add(v4);
		Expectedinside.add(v6);
		Expectedinside.add(v10);
		Expectedinside.add(v5);
		Expectedinside.add(v7);
		Expected.add(Expectedinside);
		Expectedinside = new ArrayList<Vertex>();
		Expectedinside.add(v9);
		Expectedinside.add(v4);
		Expectedinside.add(v6);
		Expectedinside.add(v10);
		Expectedinside.add(v5);
		Expectedinside.add(v7);
		Expected.add(Expectedinside);
		Expectedinside = new ArrayList<Vertex>();
		Expectedinside.add(v10);
		Expected.add(Expectedinside);

		for (List<Vertex> EpectedList : Expected) {
			assertTrue(test.contains(EpectedList));
		}
	}

	// Testing BFS for Matrix
	@Test
	public void M_testBFSEmpty() {
		Set test = Algorithms.BreadthFirstSearch(M_testgraph_empty);
		
		assertEquals(test.size(), 0);
	}

	@Test
	public void M_testBFSOneEdge() {
		Set<List<Vertex>> test = Algorithms.BreadthFirstSearch(M_testgraph_one);
		Queue<Vertex> Expected = new LinkedList<Vertex>();
		
		Expected.add(v1);
		Expected.add(v2);
		Expected.add(v2);
		
		assertEquals(test.size(), 2);

		for (List<Vertex> ResultList : test) {
			for (Vertex ResultElement : ResultList) {
				assertTrue(ResultElement.equals(Expected.remove()));
			}
		}
	}

	@Test
	public void M_testBFSTwoLoopingEdges() {
		Set<List<Vertex>> test = Algorithms.BreadthFirstSearch(M_testgraph_two);
		Queue<Vertex> Expected = new LinkedList<Vertex>();

		Expected.add(v2);
		Expected.add(v1);
		Expected.add(v1);
		Expected.add(v2);

		assertEquals(test.size(), 2);

		for (List<Vertex> ResultList : test) {
			for (Vertex ResultElement : ResultList) {
				assertTrue(ResultElement.equals(Expected.remove()));
			}

		}

	}

	@Test
	public void M_testBFSTwoBranches() {
		Set<List<Vertex>> test = Algorithms.BreadthFirstSearch(M_testgraph_three);
		Queue<Vertex> Expected = new LinkedList<Vertex>();
		
		Expected.add(v1);
		Expected.add(v2);
		Expected.add(v3);
		Expected.add(v2);
		Expected.add(v3);
		
		assertEquals(test.size(), 3);

		for (List<Vertex> ResultList : test) {
			for (Vertex ResultElement : ResultList) {
				assertTrue(ResultElement.equals(Expected.remove()));
			}
		}
	}

	@Test
	public void M_testBFSMultipleBranches() {
		Set<List<Vertex>> test = Algorithms.BreadthFirstSearch(M_testgraph_four);
		List<List<Vertex>> Expected = new ArrayList<List<Vertex>>();
		List<Vertex> Expectedinside = new ArrayList<Vertex>();

		assertEquals(test.size(), 5);
		
		Expectedinside.add(v5);
		Expected.add(Expectedinside);
		Expectedinside = new ArrayList<Vertex>();
		Expectedinside.add(v4);
		Expected.add(Expectedinside);
		Expectedinside = new ArrayList<Vertex>();
		Expectedinside.add(v3);
		Expected.add(Expectedinside);
		Expectedinside = new ArrayList<Vertex>();
		Expectedinside.add(v1);
		Expectedinside.add(v2);
		Expectedinside.add(v3);
		Expectedinside.add(v4);
		Expectedinside.add(v5);
		Expected.add(Expectedinside);
		Expectedinside = new ArrayList<Vertex>();
		Expectedinside.add(v2);
		Expectedinside.add(v3);
		Expectedinside.add(v4);
		Expectedinside.add(v5);
		Expected.add(Expectedinside);

		for (List<Vertex> EpectedList : Expected) {
			assertTrue(test.contains(EpectedList));
		}
	}

	@Test
	public void M_testBFSMultiStart() {
		Set<List<Vertex>> test = Algorithms.BreadthFirstSearch(M_testgraph_seven);
		List<List<Vertex>> Expected = new ArrayList<List<Vertex>>();
		List<Vertex> Expectedinside = new ArrayList<Vertex>();

		assertEquals(test.size(), 10);
		
		Expectedinside.add(v1);
		Expectedinside.add(v2);
		Expectedinside.add(v3);
		Expectedinside.add(v4);
		Expectedinside.add(v5);
		Expectedinside.add(v6);
		Expectedinside.add(v7);
		Expected.add(Expectedinside);
		Expectedinside = new ArrayList<Vertex>();
		Expectedinside.add(v2);
		Expectedinside.add(v3);
		Expectedinside.add(v4);
		Expectedinside.add(v5);
		Expectedinside.add(v6);
		Expectedinside.add(v7);
		Expected.add(Expectedinside);
		Expectedinside = new ArrayList<Vertex>();
		Expectedinside.add(v3);
		Expected.add(Expectedinside);
		Expectedinside = new ArrayList<Vertex>();
		Expectedinside.add(v4);
		Expectedinside.add(v5);
		Expectedinside.add(v6);
		Expectedinside.add(v7);
		Expected.add(Expectedinside);
		Expectedinside = new ArrayList<Vertex>();
		Expectedinside.add(v6);
		Expectedinside.add(v7);
		Expected.add(Expectedinside);
		Expectedinside = new ArrayList<Vertex>();
		Expectedinside.add(v7);
		Expected.add(Expectedinside);
		Expectedinside = new ArrayList<Vertex>();
		Expectedinside.add(v8);
		Expectedinside.add(v9);
		Expectedinside.add(v4);
		Expectedinside.add(v6);
		Expectedinside.add(v10);
		Expectedinside.add(v5);
		Expectedinside.add(v7);
		Expected.add(Expectedinside);
		Expectedinside = new ArrayList<Vertex>();
		Expectedinside.add(v9);
		Expectedinside.add(v4);
		Expectedinside.add(v6);
		Expectedinside.add(v10);
		Expectedinside.add(v5);
		Expectedinside.add(v7);
		Expected.add(Expectedinside);
		Expectedinside = new ArrayList<Vertex>();
		Expectedinside.add(v10);
		Expected.add(Expectedinside);

		for (List<Vertex> EpectedList : Expected) {
			assertTrue(test.contains(EpectedList));
		}
	}
	
	// DFS Testing for List

	@Test
	public void testDFSultipleBranches() {
		Set<List<Vertex>> test = Algorithms.DepthFirstSearch(testgraph_four);
		List<List<Vertex>> Expected = new ArrayList<List<Vertex>>();
		List<Vertex> Expectedinside = new ArrayList<Vertex>();

		assertEquals(test.size(), 5);
		
		Expectedinside.add(v5);
		Expected.add(Expectedinside);
		Expectedinside = new ArrayList<Vertex>();
		Expectedinside.add(v4);
		Expected.add(Expectedinside);
		Expectedinside = new ArrayList<Vertex>();
		Expectedinside.add(v3);
		Expected.add(Expectedinside);
		Expectedinside = new ArrayList<Vertex>();
		Expectedinside.add(v1);
		Expectedinside.add(v2);
		Expectedinside.add(v3);
		Expectedinside.add(v4);
		Expectedinside.add(v5);
		Expected.add(Expectedinside);
		Expectedinside = new ArrayList<Vertex>();
		Expectedinside.add(v2);
		Expectedinside.add(v3);
		Expectedinside.add(v4);
		Expectedinside.add(v5);
		Expected.add(Expectedinside);

		for (List<Vertex> EpectedList : Expected) {
			assertTrue(test.contains(EpectedList));
		}
	}

	@Test
	public void testDFSMultiStart() {
		Set<List<Vertex>> test = Algorithms.DepthFirstSearch(testgraph_seven);
		List<List<Vertex>> Expected = new ArrayList<List<Vertex>>();
		List<Vertex> Expectedinside = new ArrayList<Vertex>();

		assertEquals(test.size(), 10);
		
		Expectedinside.add(v1);
		Expectedinside.add(v2);
		Expectedinside.add(v3);
		Expectedinside.add(v4);
		Expectedinside.add(v5);
		Expectedinside.add(v6);
		Expectedinside.add(v7);
		Expected.add(Expectedinside);
		Expectedinside = new ArrayList<Vertex>();
		Expectedinside.add(v2);
		Expectedinside.add(v3);
		Expectedinside.add(v4);
		Expectedinside.add(v5);
		Expectedinside.add(v6);
		Expectedinside.add(v7);
		Expected.add(Expectedinside);
		Expectedinside = new ArrayList<Vertex>();
		Expectedinside.add(v3);
		Expected.add(Expectedinside);
		Expectedinside = new ArrayList<Vertex>();
		Expectedinside.add(v4);
		Expectedinside.add(v5);
		Expectedinside.add(v6);
		Expectedinside.add(v7);
		Expected.add(Expectedinside);
		Expectedinside = new ArrayList<Vertex>();
		Expectedinside.add(v5);
		Expected.add(Expectedinside);
		Expectedinside = new ArrayList<Vertex>();
		Expectedinside.add(v6);
		Expectedinside.add(v7);
		Expected.add(Expectedinside);
		Expectedinside = new ArrayList<Vertex>();
		Expectedinside.add(v7);
		Expected.add(Expectedinside);
		Expectedinside = new ArrayList<Vertex>();
		Expectedinside.add(v8);
		Expectedinside.add(v9);
		Expectedinside.add(v4);
		Expectedinside.add(v5);
		Expectedinside.add(v6);
		Expectedinside.add(v7);
		Expectedinside.add(v10);
		Expected.add(Expectedinside);
		Expectedinside = new ArrayList<Vertex>();
		Expectedinside.add(v9);
		Expectedinside.add(v4);
		Expectedinside.add(v5);
		Expectedinside.add(v6);
		Expectedinside.add(v7);
		Expectedinside.add(v10);
		Expected.add(Expectedinside);
		Expectedinside = new ArrayList<Vertex>();
		Expectedinside.add(v10);
		Expected.add(Expectedinside);

		for (List<Vertex> EpectedList : Expected) {
			assertTrue(test.contains(EpectedList));
		}
	}

	// DFS Testing for Matrix

	@Test
	public void M_testDFSultipleBranches() {
		Set<List<Vertex>> test = Algorithms.DepthFirstSearch(M_testgraph_four);
		List<List<Vertex>> Expected = new ArrayList<List<Vertex>>();
		List<Vertex> Expectedinside = new ArrayList<Vertex>();

		assertEquals(test.size(), 5);
		
		Expectedinside.add(v5);
		Expected.add(Expectedinside);
		Expectedinside = new ArrayList<Vertex>();
		Expectedinside.add(v4);
		Expected.add(Expectedinside);
		Expectedinside = new ArrayList<Vertex>();
		Expectedinside.add(v3);
		Expected.add(Expectedinside);
		Expectedinside = new ArrayList<Vertex>();
		Expectedinside.add(v1);
		Expectedinside.add(v2);
		Expectedinside.add(v3);
		Expectedinside.add(v4);
		Expectedinside.add(v5);
		Expected.add(Expectedinside);
		Expectedinside = new ArrayList<Vertex>();
		Expectedinside.add(v2);
		Expectedinside.add(v3);
		Expectedinside.add(v4);
		Expectedinside.add(v5);
		Expected.add(Expectedinside);

		for (List<Vertex> EpectedList : Expected) {
			assertTrue(test.contains(EpectedList));
		}

	}

	@Test
	public void M_testDFSMultiStart() {
		Set<List<Vertex>> test = Algorithms.DepthFirstSearch(M_testgraph_seven);
		List<List<Vertex>> Expected = new ArrayList<List<Vertex>>();
		List<Vertex> Expectedinside = new ArrayList<Vertex>();

		assertEquals(test.size(), 10);

		Expectedinside.add(v1);
		Expectedinside.add(v2);
		Expectedinside.add(v3);
		Expectedinside.add(v4);
		Expectedinside.add(v5);
		Expectedinside.add(v6);
		Expectedinside.add(v7);
		Expected.add(Expectedinside);
		Expectedinside = new ArrayList<Vertex>();
		Expectedinside.add(v2);
		Expectedinside.add(v3);
		Expectedinside.add(v4);
		Expectedinside.add(v5);
		Expectedinside.add(v6);
		Expectedinside.add(v7);
		Expected.add(Expectedinside);
		Expectedinside = new ArrayList<Vertex>();
		Expectedinside.add(v3);
		Expected.add(Expectedinside);
		Expectedinside = new ArrayList<Vertex>();
		Expectedinside.add(v4);
		Expectedinside.add(v5);
		Expectedinside.add(v6);
		Expectedinside.add(v7);
		Expected.add(Expectedinside);
		Expectedinside = new ArrayList<Vertex>();
		Expectedinside.add(v5);
		Expected.add(Expectedinside);
		Expectedinside = new ArrayList<Vertex>();
		Expectedinside.add(v6);
		Expectedinside.add(v7);
		Expected.add(Expectedinside);
		Expectedinside = new ArrayList<Vertex>();
		Expectedinside.add(v7);
		Expected.add(Expectedinside);
		Expectedinside = new ArrayList<Vertex>();
		Expectedinside.add(v8);
		Expectedinside.add(v9);
		Expectedinside.add(v4);
		Expectedinside.add(v5);
		Expectedinside.add(v6);
		Expectedinside.add(v7);
		Expectedinside.add(v10);
		Expected.add(Expectedinside);
		Expectedinside = new ArrayList<Vertex>();
		Expectedinside.add(v9);
		Expectedinside.add(v4);
		Expectedinside.add(v5);
		Expectedinside.add(v6);
		Expectedinside.add(v7);
		Expectedinside.add(v10);
		Expected.add(Expectedinside);
		Expectedinside = new ArrayList<Vertex>();
		Expectedinside.add(v10);
		Expected.add(Expectedinside);

		for (List<Vertex> EpectedList : Expected) {
			assertTrue(test.contains(EpectedList));
		}
	}

	// Testing Upstream for List

	@Test
	public void testcommonUpstreamOne() {
		List<Vertex> test = Algorithms.commonUpstream(testgraph_four, v5, v4);
		List<Vertex> Expected = new ArrayList<Vertex>();
		
		Expected.add(v2);
		
		assertTrue(test.equals(Expected));
	}

	@Test
	public void testcommonUpstreamNone() {
		List<Vertex> test = Algorithms.commonUpstream(testgraph_four, v2, v4);
		List<Vertex> Expected = new ArrayList<Vertex>();
		
		assertTrue(test.equals(Expected));
	}

	@Test
	public void testcommonUpstreamMultiple() {
		List<Vertex> test = Algorithms.commonUpstream(testgraph_five, v2, v3);
		List<Vertex> Expected = new ArrayList<Vertex>();
		
		Expected.add(v1);
		Expected.add(v4);
		
		assertTrue(test.equals(Expected));
	}

	// Testing Upstream for matrix

	@Test
	public void M_testcommonUpstreamOne() {
		List<Vertex> test = Algorithms.commonUpstream(M_testgraph_four, v5, v4);
		List<Vertex> Expected = new ArrayList<Vertex>();
		
		Expected.add(v2);
		
		assertTrue(test.equals(Expected));
	}

	@Test
	public void M_testcommonUpstreamNone() {
		List<Vertex> test = Algorithms.commonUpstream(M_testgraph_four, v2, v4);
		List<Vertex> Expected = new ArrayList<Vertex>();
		
		assertTrue(test.equals(Expected));
	}

	@Test
	public void M_testcommonUpstreamMultiple() {
		List<Vertex> test = Algorithms.commonUpstream(M_testgraph_five, v2, v3);
		List<Vertex> Expected = new ArrayList<Vertex>();
		
		Expected.add(v1);
		Expected.add(v4);
		
		assertTrue(test.equals(Expected));
	}

	// Testing Common Downstream for list

	@Test
	public void commonDownstreamTestNone() {
		List<Vertex> test = Algorithms.commonDownstream(testgraph_seven, v2, v8);
		List<Vertex> Expected = new ArrayList<Vertex>();
		
		assertTrue(test.equals(Expected));
	}

	@Test
	public void commonDownstreamTestOne() {
		List<Vertex> test = Algorithms.commonDownstream(testgraph_seven, v2, v9);
		List<Vertex> Expected = new ArrayList<Vertex>();

		Expected.add(v4);
		
		assertTrue(test.equals(Expected));
	}

	@Test
	public void commonDownstreamTestMultiple() {
		List<Vertex> test = Algorithms.commonDownstream(testgraph_five, v1, v4);
		List<Vertex> Expected = new ArrayList<Vertex>();

		Expected.add(v2);
		Expected.add(v3);
		
		assertTrue(test.equals(Expected));
	}

	// Testing Common Downstream for matrix

	@Test
	public void M_commonDownstreamTestNone() {
		List<Vertex> test = Algorithms.commonDownstream(M_testgraph_seven, v2, v8);
		List<Vertex> Expected = new ArrayList<Vertex>();
		
		assertTrue(test.equals(Expected));
	}

	@Test
	public void M_commonDownstreamTestOne() {
		List<Vertex> test = Algorithms.commonDownstream(M_testgraph_seven, v2, v9);
		List<Vertex> Expected = new ArrayList<Vertex>();

		Expected.add(v4);
		
		assertTrue(test.equals(Expected));

	}

	@Test
	public void M_commonDownstreamTestMultiple() {
		List<Vertex> test = Algorithms.commonDownstream(M_testgraph_five, v1, v4);
		List<Vertex> Expected = new ArrayList<Vertex>();

		Expected.add(v2);
		Expected.add(v3);
		
		assertTrue(test.equals(Expected));

	}

	// Testing Shortest Path for List
	@Test
	public void testShortestPath() throws PathNotFoundException {
		int test = Algorithms.shortestDistance(testgraph_four, v1, v5);

		assertEquals(test, 2);

	}

	@Test
	public void testShortestPathLong() throws PathNotFoundException {
		int test = Algorithms.shortestDistance(testgraph_six, v1, v6);

		assertEquals(test, 3);

	}

	@Test
	public void testShortestPathMultiStartGraphatstartandEnd() throws PathNotFoundException {
		int test = Algorithms.shortestDistance(testgraph_seven, v1, v7);

		assertEquals(test, 4);

	}

	@Test
	public void testShortestPathMultiStartGraphSamePoint() throws PathNotFoundException {
		int test = Algorithms.shortestDistance(testgraph_seven, v1, v1);

		assertEquals(test, 0);

	}

	@Test
	public void testShortestPathMultiStartGraphSameRandomPoints() throws PathNotFoundException {
		int test = Algorithms.shortestDistance(testgraph_seven, v8, v5);

		assertEquals(test, 3);

	}

	@Test
	public void testShortestPathMultiStartGraphSameRandomPointstwo() throws PathNotFoundException {
		int test = Algorithms.shortestDistance(testgraph_seven, v1, v5);

		assertEquals(test, 3);

	}
	
	@Test (expected = PathNotFoundException.class)
	public void testShortestPathMultiStartGraphException() throws PathNotFoundException {
		int test = Algorithms.shortestDistance(testgraph_seven, v1, v10);
	}

	// Testing Shortest Path for matrix
	@Test
	public void M_testShortestPath() throws PathNotFoundException {
		int test = Algorithms.shortestDistance(M_testgraph_four, v1, v5);

		assertEquals(test, 2);

	}

	@Test
	public void M_testShortestPathLong() throws PathNotFoundException {
		int test = Algorithms.shortestDistance(M_testgraph_six, v1, v6);

		assertEquals(test, 3);

	}

	@Test
	public void M_testShortestPathMultiStartGraphatstartandEnd() throws PathNotFoundException {
		int test = Algorithms.shortestDistance(M_testgraph_seven, v1, v7);

		assertEquals(test, 4);

	}

	@Test
	public void M_testShortestPathMultiStartGraphSamePoint() throws PathNotFoundException {
		int test = Algorithms.shortestDistance(M_testgraph_seven, v1, v1);

		assertEquals(test, 0);

	}

	@Test
	public void M_testShortestPathMultiStartGraphSameRandomPoints() throws PathNotFoundException {
		int test = Algorithms.shortestDistance(M_testgraph_seven, v8, v5);

		assertEquals(test, 3);

	}

	@Test
	public void M_testShortestPathMultiStartGraphSameRandomPointstwo() throws PathNotFoundException {
		int test = Algorithms.shortestDistance(M_testgraph_seven, v1, v5);

		assertEquals(test, 3);
	}
	
	@Test (expected = PathNotFoundException.class)
	public void M_testShortestPathMultiStartGraphException() throws PathNotFoundException {
		int test = Algorithms.shortestDistance(M_testgraph_seven, v1, v10);
	}

}
