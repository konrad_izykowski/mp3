/**
 * 
 * This Algorithm Class contains Breadth First Search, Depth First Search, Shortest Path,
 * Common Upstream and Common Downstream Algorithms.
 * @author Konrad Izykowski 26506148 
 * @author Zeyad Tamimi 12504149
 *
 */
package ca.ubc.ece.cpen221.mp3.graph;

import java.util.*;

import ca.ubc.ece.cpen221.mp3.staff.Graph;
import ca.ubc.ece.cpen221.mp3.staff.Vertex;

public class Algorithms {

	/**
	 * *********************** Algorithms ****************************
	 */

	
	
	/**
	 * This method implements a shortest distance algorithm to be used on an unweighted graph
	 * The algorithm takes in a staring vertex a and and ending vertex b that are in graph
	 * and return the  number of edges between them as an integer (i.e the distance in an unweighted graph)
	 * id the starting Vertex is also the ending Vertex then the method would return 0, ad if no path was fownd from a to b a NoPathException
	 * is thrown
	 * 
	 * @param graph - The unweighted Graph to Find the shortest Path in
	 * @param a - The starting Vertex of the shortest path search, MUST BE IN graph
	 * @param b - The Ending Vertex of the shortest path search, MUST BE IN graph
	 * @return - The number of edges (distance) between Vertex a and Vertex b as an integer , if a = b then return will be 0
	 * @throws PathNotFoundException - if no path was found from a to b
	 */
	public static int shortestDistance(Graph graph, Vertex a, Vertex b) throws PathNotFoundException {
		
		//Initializing the collections that will be used for the algorithm
		Map<Vertex, Integer> DistVertexMap = new HashMap<Vertex, Integer>();
		Queue<Vertex> toSearchQueue = new LinkedList<Vertex>();
		Set<Vertex> VisitedVertex = new HashSet<Vertex>();
		
		Vertex currentVertex = a;
		
		//Setting the distance of all vertices to -1 to indicate that they have not been assigned a distance yet.
		for (Vertex v : graph.getVertices()) DistVertexMap.put(v , -1);
		
		VisitedVertex.add(currentVertex);
		toSearchQueue.add(currentVertex);
		DistVertexMap.put(currentVertex , 0);
		
		while (!toSearchQueue.isEmpty())
		{
			currentVertex = toSearchQueue.remove();
			
			//If we hit b before reaching the end of the BFS search then break from the search
			if ( currentVertex.equals(b))
				break;
			else
			{
				//Loops through all downstream elements of the Queued Vertex and assigns them a distance.
				for (Vertex nextVertex : graph.getDownstreamNeighbors(currentVertex))
				{
					if (!VisitedVertex.contains(nextVertex))
					{
						toSearchQueue.add(nextVertex);
						VisitedVertex.add(nextVertex);
						DistVertexMap.put(nextVertex , DistVertexMap.get(currentVertex) + 1);
					}							
				}	
			}
		}
		
		if (!currentVertex.equals(b) || DistVertexMap.get(b) == -1 )
	        throw new PathNotFoundException();

	   return DistVertexMap.get(b);
	}
	
	
	/**
	 * This method implements a Breadth First Search algorithm on a provided graph to traverse all possible paths
	 * and to produce a set containing lists of vertices that are produced by applying the BSF algorithm
	 * to every Vertex in the graph
	 * 
	 * @param graph - The Graph to implement breadth first search
	 * @return - An unordered set contain lists of vertices, where each list is the result of applying the BFS algorithm to a different Vertex in the graph  
	 */
	
	public static Set<List<Vertex>> BreadthFirstSearch(Graph graphtoSearch)
	{
		//Initializing the collections that will be used for the algorithm
		Set<List<Vertex>> TraversedSet = new HashSet<List<Vertex>>();
		Queue<Vertex> toSearchQueue = new LinkedList<Vertex>();
		Set<Vertex> VisitedVertex = new HashSet<Vertex>();
		List<Vertex> VertexList;
		
		Vertex toTest;
		
		// Loops through every vertex in the graph as a starting point to the algorithm
		for (Vertex startingVertex : graphtoSearch.getVertices())
		{
			
			VertexList= new ArrayList<Vertex>();
			VisitedVertex = new HashSet<Vertex>();
			
			toSearchQueue.add(startingVertex);
			VisitedVertex.add(startingVertex);
			
			while (!toSearchQueue.isEmpty())
			{
				toTest = toSearchQueue.remove();
				VertexList.add(toTest);
				
				for(Vertex v : graphtoSearch.getDownstreamNeighbors(toTest))
				{
					if(!VisitedVertex.contains(v))
					{
						toSearchQueue.add(v);
						VisitedVertex.add(v);
					}	
				}
			}
			
			TraversedSet.add(VertexList);
		}
		
		return  TraversedSet;
	}
	
	
	/**
	 * This method implements a Depth First Search algorithm on a provided graph to traverse all possible paths
	 * and to produce a set containing lists of vertices that are produced by applying the BSF algorithm
	 * to every Vertex in the graph
	 * 
	 * @param graph - The Graph to implement depth first search
	 * @return - An unordered set contain lists of vertices, where each list is the result of applying the DFS algorithm to a different Vertex in the graph  
	 */
	public static Set<List<Vertex>> DepthFirstSearch(Graph graphtoSearch)
	{
		//Initializing the collections that will be used for the algorithm
		Set<List<Vertex>> TraversedSet = new HashSet<List<Vertex>>();
		Stack<Vertex> toSearchStack = new Stack<Vertex>();
		Set<Vertex> VisitedVertexSet = new HashSet<Vertex>();
		List<Vertex> VertexList = new ArrayList<Vertex>();
		
		Vertex toTest;
		
		// Loops through every vertex in the graph as a starting point to the algorithm
		for (Vertex startingVertex : graphtoSearch.getVertices())
		{
			
			VisitedVertexSet = new HashSet<Vertex>();
			VertexList = new ArrayList<Vertex>();
			
			toSearchStack.add(startingVertex);
			VisitedVertexSet.add(startingVertex);
			VertexList.add(startingVertex);
			
			while(!toSearchStack.isEmpty())
			{
				toTest = UnvisitedDownstream(graphtoSearch.getDownstreamNeighbors(toSearchStack.peek()) , VisitedVertexSet);
				
				if (toTest != null )
				{
					VertexList.add(toTest);
					VisitedVertexSet.add(toTest);
					
					if (graphtoSearch.getDownstreamNeighbors(toSearchStack.peek()).size() > 0)
						toSearchStack.add(toTest);
				}
				else
				{
					toSearchStack.pop();
				}
			}
			TraversedSet.add(VertexList);
		}
		
		return  TraversedSet;
	}
	

	/**
	 * A helper method that takes in a list of vertices to visit and a set of visited vertices, 
	 * then returns the first element of the list that has not been visited before. If all elements in the list have been visited before
	 * return a null value.
	 * 
	 * @param verticiesToVIsit - The list of vertices to check
	 * @param visitedVertexSet - a set containing vertices that have already been visited
	 * @return - The first vertex in the verticiesToVIsit that has not been visited before, if all vertices in the list have been visited
	 * 			 return null
	 */
	private static Vertex UnvisitedDownstream(List<Vertex> verticiesToVIsit, Set<Vertex> visitedVertexSet) 
	{
		for (Vertex vertextocheck : verticiesToVIsit)
		{
			if (!visitedVertexSet.contains(vertextocheck))
				return vertextocheck;
		}
		
		return null;
	}
    
	
	/**
	 * Returns all common Upstream vertices between the  vertex a and b as a list, 
	 * if there are no common upstream vertices between the two, an empty list is returned.
	 * 
	 * @param graph - the graph to apply the algorithm on
	 * @param a - The first vertex to check 
	 * @param b - The second vertex to check 
	 * @return - A list of all upstream vertices that are common to both a and b an empty list will be returned if they don't share any Upstream vertices in common.
	 */
    public static List<Vertex> commonUpstream(Graph graph, Vertex a, Vertex b)
    {
    	//Initializing the collections that will be used for the algorithm
        List<Vertex> upstreamA = graph.getUpstreamNeighbors(a);
        List<Vertex> upstreamB = graph.getUpstreamNeighbors(b);
        List<Vertex> aAndB = new ArrayList<Vertex>();
        
        for(Vertex currentA : upstreamA){
            for(Vertex currentB : upstreamB){
                if(currentA.equals(currentB)){
                    aAndB.add(currentA);
                }
            }
        }
        
        return aAndB;
    }
    
    /**
	 * Returns all common Downstream vertices between the vertex a and b as a list, 
	 * if there are no common Downstream vertices between the two, an empty list is returned.
	 * 
	 * @param graph - the graph to apply the algorithm on
	 * @param a - The first vertex to check 
	 * @param b - The second vertex to check 
	 * @return - A list of all Downstream vertices that are common to both a and b, an empty list will be returned if they don't share any Downstream vertices in common.
	 */
    public static List<Vertex> commonDownstream(Graph graph, Vertex a, Vertex b)
    {
    	//Initializing the collections that will be used for the algorithm
        List<Vertex> downstreamA = graph.getDownstreamNeighbors(a);
        List<Vertex> downstreamB = graph.getDownstreamNeighbors(b);
        List<Vertex> aAndB = new ArrayList<Vertex>();
        
        for(Vertex currentA : downstreamA){
            for(Vertex currentB : downstreamB){
                if(currentA.equals(currentB)){
                    aAndB.add(currentA);
                }
            }
        }
        
        return aAndB;
    }
    
}
