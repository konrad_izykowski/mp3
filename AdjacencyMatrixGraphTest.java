/**
 * 
 * This jUnit tests the AdjacencyMatrixGraph object type that implements Graph
 * and conducts various operations with that Graph object.
 * @author Konrad Izykowski 26506148 
 * @author Zeyad Tamimi 12504149
 *
 */


package ca.ubc.ece.cpen221.mp3.tests;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import ca.ubc.ece.cpen221.mp3.graph.AdjacencyMatrixGraph;
import ca.ubc.ece.cpen221.mp3.staff.Graph;
import ca.ubc.ece.cpen221.mp3.staff.Vertex;

public class AdjacencyMatrixGraphTest {
    
    private Graph tester = new AdjacencyMatrixGraph();
    private List<Vertex> output = new ArrayList<Vertex>();
    private Vertex v1 = new Vertex("v1");
    private Vertex v2 = new Vertex("v2");
    private Vertex v3 = new Vertex("v3");
    private Vertex v4 = new Vertex("v4");
    private Vertex v5 = new Vertex("v5");
    
    @Test
    public void addingVerticesShouldAddThem() {
        tester.addVertex(v1);
        tester.addVertex(v3);
        tester.addVertex(v2);
        
        output = tester.getVertices();
        
        assertTrue(output.contains(v1));
        assertTrue(output.contains(v2));
        assertTrue(output.contains(v3));
        assertFalse(output.contains(v4));
        
    }
    
    @Test
    public void addingEdgesShouldAddThem() {
        tester.addVertex(v1);
        tester.addVertex(v3);
        tester.addVertex(v2);
        
        tester.addEdge(v1, v2);
        tester.addEdge(v1, v3);
        tester.addEdge(v2, v1);
        tester.addEdge(v3, v3);
        
        assertTrue(tester.edgeExists(v1, v2));
        assertTrue(tester.edgeExists(v1, v3));
        assertTrue(tester.edgeExists(v2, v1));
        assertTrue(tester.edgeExists(v3, v3));

        assertFalse(tester.edgeExists(v1, v1));
        assertFalse(tester.edgeExists(v2, v2));
        assertFalse(tester.edgeExists(v2, v3));
        assertFalse(tester.edgeExists(v3, v1));
        assertFalse(tester.edgeExists(v3, v2));
    }
    
    @Test
    public void getUpstreamNeighboursShouldReturnUpstreamNeighbours() {
        tester.addVertex(v1);
        tester.addVertex(v3);
        tester.addVertex(v2);
        tester.addVertex(v4);
        tester.addVertex(v5);

        tester.addEdge(v1, v2);
        tester.addEdge(v1, v3);
        tester.addEdge(v2, v2);
        tester.addEdge(v2, v4);
        tester.addEdge(v4, v1);
        tester.addEdge(v2, v3);
        
        assertFalse(tester.getUpstreamNeighbors(v1).contains(v1));
        assertFalse(tester.getUpstreamNeighbors(v1).contains(v2));
        assertFalse(tester.getUpstreamNeighbors(v1).contains(v3));
        assertTrue(tester.getUpstreamNeighbors(v1).contains(v4));

        assertTrue(tester.getUpstreamNeighbors(v2).contains(v1));
        assertTrue(tester.getUpstreamNeighbors(v2).contains(v2));
        assertFalse(tester.getUpstreamNeighbors(v2).contains(v3));
        assertFalse(tester.getUpstreamNeighbors(v2).contains(v4));
        
        assertTrue(tester.getUpstreamNeighbors(v3).contains(v1));
        assertTrue(tester.getUpstreamNeighbors(v3).contains(v2));
        assertFalse(tester.getUpstreamNeighbors(v3).contains(v3));
        assertFalse(tester.getUpstreamNeighbors(v3).contains(v4));
        
        assertFalse(tester.getUpstreamNeighbors(v4).contains(v1));
        assertTrue(tester.getUpstreamNeighbors(v4).contains(v2));
        assertFalse(tester.getUpstreamNeighbors(v4).contains(v3));
        assertFalse(tester.getUpstreamNeighbors(v4).contains(v4));

        assertEquals(0, tester.getUpstreamNeighbors(v5).size());
    }
    
    @Test
    public void getDownStreamNeighboursShouldReturnDownStreamNeighbours() {tester.addVertex(v1);
        tester.addVertex(v3);
        tester.addVertex(v2);
        tester.addVertex(v4);
        tester.addVertex(v5);
    
        tester.addEdge(v1, v2);
        tester.addEdge(v1, v3);
        tester.addEdge(v2, v2);
        tester.addEdge(v2, v4);
        tester.addEdge(v4, v1);
        tester.addEdge(v2, v3);
        
        assertFalse(tester.getDownstreamNeighbors(v1).contains(v1));
        assertTrue(tester.getDownstreamNeighbors(v1).contains(v2));
        assertTrue(tester.getDownstreamNeighbors(v1).contains(v3));
        assertFalse(tester.getDownstreamNeighbors(v1).contains(v4));
        
        assertFalse(tester.getDownstreamNeighbors(v2).contains(v1));
        assertTrue(tester.getDownstreamNeighbors(v2).contains(v2));
        assertTrue(tester.getDownstreamNeighbors(v2).contains(v3));
        assertTrue(tester.getDownstreamNeighbors(v2).contains(v4));
        
        assertEquals(0, tester.getDownstreamNeighbors(v3).size());
        
        assertTrue(tester.getDownstreamNeighbors(v4).contains(v1));
        assertFalse(tester.getDownstreamNeighbors(v4).contains(v2));
        assertFalse(tester.getDownstreamNeighbors(v4).contains(v3));
        assertFalse(tester.getDownstreamNeighbors(v4).contains(v4));
    
        assertEquals(0, tester.getDownstreamNeighbors(v5).size());
    }
    
    @Test
    public void emptyGraphGetVertexesShouldBeSizeZero() {
        assertEquals(tester.getVertices().size(), 0);
    }
    
}