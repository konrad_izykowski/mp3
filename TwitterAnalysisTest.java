/**
 * 
 * This jUnit tests TwitterAnalysis.java which reads data from a file, 
 * reads queries from a file, and writes answers to a file
 * @author Konrad Izykowski 26506148 
 * @author Zeyad Tamimi 12504149
 *
 */


package ca.ubc.ece.cpen221.mp3.tests;

import static org.junit.Assert.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

import org.junit.Test;

import ca.ubc.ece.cpen221.mp3.graph.TwitterAnalysis;

public class TwitterAnalysisTest {
    Path generated, expected;
    byte[] bytesGenerated, bytesExpected;
    
    @Test
    public void noOutputForEmptyData() {
        TwitterAnalysis.main(new String[] {"testdata/0data.txt", 
                        "testdata/0query.txt", "testdata/0answer.txt"});
        
        generated = Paths.get("testdata/0answer.txt");
        expected = Paths.get("testdata/0expected.txt");
        try {
            bytesGenerated = Files.readAllBytes(generated);
            bytesExpected = Files.readAllBytes(expected);
            
            assertTrue(Arrays.equals(bytesGenerated, bytesExpected));
            
        } catch (IOException e) {
            throw new RuntimeException();
        }
    }
    
    @Test
    public void checkCommonInfluencersWorks() {
        TwitterAnalysis.main(new String[] {"testdata/1data.txt", 
                        "testdata/1query.txt", "testdata/1answer.txt"});
        
        generated = Paths.get("testdata/1answer.txt");
        expected = Paths.get("testdata/1expected.txt");
        try {
            bytesGenerated = Files.readAllBytes(generated);
            bytesExpected = Files.readAllBytes(expected);
            
            assertTrue(Arrays.equals(bytesGenerated, bytesExpected));
            
        } catch (IOException e) {
            throw new RuntimeException();
        }
    }
    
    @Test
    public void checknumRetweetsWorks() {
        TwitterAnalysis.main(new String[] {"testdata/2data.txt", 
                        "testdata/2query.txt", "testdata/2answer.txt"});
        
        generated = Paths.get("testdata/2answer.txt");
        expected = Paths.get("testdata/2expected.txt");
        try {
            bytesGenerated = Files.readAllBytes(generated);
            bytesExpected = Files.readAllBytes(expected);
            
            assertTrue(Arrays.equals(bytesGenerated, bytesExpected));
            
        } catch (IOException e) {
            throw new RuntimeException();
        }
    }
    
    @Test
    public void checkQueryWorksWithBranching() {
        TwitterAnalysis.main(new String[] {"testdata/3data.txt", 
                        "testdata/3query.txt", "testdata/3answer.txt"});
        
        generated = Paths.get("testdata/3answer.txt");
        expected = Paths.get("testdata/3expected.txt");
        try {
            bytesGenerated = Files.readAllBytes(generated);
            bytesExpected = Files.readAllBytes(expected);
            
            assertTrue(Arrays.equals(bytesGenerated, bytesExpected));
            
        } catch (IOException e) {
            throw new RuntimeException();
        }
    }
    
    @Test
    public void checkOverLappingInfluencers() {
        TwitterAnalysis.main(new String[] {"testdata/4data.txt", 
                        "testdata/4query.txt", "testdata/4answer.txt"});
        
        generated = Paths.get("testdata/4answer.txt");
        expected = Paths.get("testdata/4expected.txt");
        try {
            bytesGenerated = Files.readAllBytes(generated);
            bytesExpected = Files.readAllBytes(expected);
            
            assertTrue(Arrays.equals(bytesGenerated, bytesExpected));
            
        } catch (IOException e) {
            throw new RuntimeException();
        }
    }

    @Test
    public void ignoreMalformedQueries1() {
        TwitterAnalysis.main(new String[] {"testdata/5data.txt", 
                        "testdata/5query.txt", "testdata/5answer.txt"});
        
        generated = Paths.get("testdata/5answer.txt");
        expected = Paths.get("testdata/5expected.txt");
        try {
            bytesGenerated = Files.readAllBytes(generated);
            bytesExpected = Files.readAllBytes(expected);
            
            assertTrue(Arrays.equals(bytesGenerated, bytesExpected));
            
        } catch (IOException e) {
            throw new RuntimeException();
        }
    }
 
    @Test
    public void ignoreMalformedQueries2() {
        TwitterAnalysis.main(new String[] {"testdata/6data.txt", 
                        "testdata/6query.txt", "testdata/6answer.txt"});
        
        generated = Paths.get("testdata/6answer.txt");
        expected = Paths.get("testdata/6expected.txt");
        try {
            bytesGenerated = Files.readAllBytes(generated);
            bytesExpected = Files.readAllBytes(expected);
            
            assertTrue(Arrays.equals(bytesGenerated, bytesExpected));
            
        } catch (IOException e) {
            throw new RuntimeException();
        }
    }
    
    @Test
    public void numRetweetsFindsShortestDistance() {
        TwitterAnalysis.main(new String[] {"testdata/7data.txt", 
                        "testdata/7query.txt", "testdata/7answer.txt"});
        
        generated = Paths.get("testdata/7answer.txt");
        expected = Paths.get("testdata/7expected.txt");
        try {
            bytesGenerated = Files.readAllBytes(generated);
            bytesExpected = Files.readAllBytes(expected);
            
            assertTrue(Arrays.equals(bytesGenerated, bytesExpected));
            
        } catch (IOException e) {
            throw new RuntimeException();
        }
    }
    
    @Test (expected = RuntimeException.class)
    public void noInputFileShouldThrowException() {
        TwitterAnalysis.main(new String[] {"bleh.txt", "blah.txt", "eurgh.txt"});
        
        generated = Paths.get("testdata/5answer.txt");
        expected = Paths.get("testdata/5expected.txt");
        try {
            bytesGenerated = Files.readAllBytes(generated);
            bytesExpected = Files.readAllBytes(expected);
            
            assertTrue(Arrays.equals(bytesGenerated, bytesExpected));
            
        } catch (IOException e) {
            throw new RuntimeException();
        }
    }

}
