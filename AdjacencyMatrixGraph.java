/**
 * 
 * This function constructs an AdjacencyMatrixGraph object that implements Graph
 * and conducts various operations with that Graph object.
 * @author Konrad Izykowski 26506148 
 * @author Zeyad Tamimi 12504149
 *
 */


package ca.ubc.ece.cpen221.mp3.graph;

import java.util.*;

import ca.ubc.ece.cpen221.mp3.staff.Graph;
import ca.ubc.ece.cpen221.mp3.staff.Vertex;

public class AdjacencyMatrixGraph implements Graph {
    private List<List<Boolean>> rows = new ArrayList<List<Boolean>>();
    private List<Vertex> indexToVertex= new ArrayList<Vertex>(); 
    private int currentSize = 0;
    
        
    /**
     * Adds a vertex to the graph.
     *  
     * @param the vertex to be added to the graph
     * @requires v is not already a vertex in the graph
     */
    public void addVertex(Vertex v){
        indexToVertex.add(v);
        currentSize++;
        
        
        List<Boolean> newRow = new ArrayList<Boolean>();
        
        for(List<Boolean> currentRow : rows){
            currentRow.add(Boolean.FALSE);
            newRow.add(Boolean.FALSE);
        }
        
        newRow.add(Boolean.FALSE);
        rows.add(newRow);
    }

    /**
     * Adds an edge from v1 to v2.
     *
     * @param v1 - the vertex where the edge starts
     * @param v2 - the vertex where the edge ends
     * @requires v1 and v2 are vertices in the graph
     */
    public void addEdge(Vertex v1, Vertex v2){
        rows.get(indexToVertex.indexOf(v1)).set(indexToVertex.indexOf(v2), true);
    }

    /**
     * Checks if there is an edge from v1 to v2.
     * 
     * @param v1 - the vertex where the edge should start
     * @param v2 - the vertex where the edge should end
     * @requires v1 and v2 are vertices in the graph
     * @returns true if there is an edge starting at v1 and ending at v2
     */
    public boolean edgeExists(Vertex v1, Vertex v2){
        return rows.get(indexToVertex.indexOf(v1)).get(indexToVertex.indexOf(v2));
    }

    /**
     * Gets an array containing all downstream vertices adjacent to v.
     *
     * @param the vertex for which all downstream vertices are retrieved
     * @requires v is a vertex in the graph
     * @returns - a List of all and only the Vertices downstream of v
     *          - the List will not have any trailing null values
     *          - these are references so the vertexes can be edited
     */
    public List<Vertex> getDownstreamNeighbors(Vertex v){
        List<Boolean> currentRow = rows.get(indexToVertex.indexOf(v));
                
        List<Vertex> downstream = new ArrayList<Vertex>();
        
        int currentSize = indexToVertex.size();
        
        for(int index = 0; index < currentSize; index++){
            if(currentRow.get(index)){            
                downstream.add(indexToVertex.get(index));
            }
        }
        
        return downstream;
    }

    /**
     * Get an array containing all upstream vertices adjacent to v.
     *
     * @param the vertex for which all upstream vertices are retrieved
     * @requires v is a vertex in the graph
     * @returns - a List of all and only the Vertices upstream of v
     *          - the List will not have any trailing null values
     *          - these are references so the vertexes can be edited
     */
    public List<Vertex> getUpstreamNeighbors(Vertex v){
        int columnIndex = indexToVertex.indexOf(v);

        List<Boolean> currentRow = new ArrayList<Boolean>();
        List<Vertex> upstream = new ArrayList<Vertex>();
        
        for(int index = 0; index < currentSize; index++){
            currentRow = rows.get(index);
            if(currentRow.get(columnIndex)){
                upstream.add(indexToVertex.get(index));
            }
        }
                
        return upstream;
    }

    /**
     * Get all vertices in the graph.
     *
     * @returns - returns a list containing all and only the vertices in the graph
     *          - the list will have no trailing null values
     *          - these are references so the vertexes can be edited
     */
    public List<Vertex> getVertices(){
        return indexToVertex;
    }
}
